<?php

namespace Drupal\Tests\external_reset_password\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests of reset user's password.
 *
 * @group external_reset_password
 */
class ResetPasswordTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['user', 'external_reset_password'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser(['administer site configuration']);
  }

  /**
   * Tests redirection to an external reset password website.
   */
  public function testExternalPageRedirect() {
    $this->setExternalResetPasswordUrl('http://example.com');
    $this->drupalGet('/user/password');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('http://example.com');
  }

  /**
   * Tests the behavior of external page redirection with an empty destination.
   */
  public function testEmptyExternalPage() {
    $this->setExternalResetPasswordUrl('');
    $this->drupalGet('/user/password');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('/user/password');
  }

  /**
   * Set the URL for an external password reset.
   *
   * @param string $url
   *   The URL to set.
   */
  protected function setExternalResetPasswordUrl(string $url): void {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/people/external-reset-password/settings');
    $this->getSession()->getPage()->fillField('url', $url);
    $this->submitForm([], 'Save configuration');
    $this->drupalLogout();
  }

}
