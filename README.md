## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

The External Reset Password module provides administrators with the flexibility
to configure an external path for the user reset password page in Drupal. When
users initiate a password reset, they will be redirected to the specified
external resource, enhancing customization and control over the password 
recovery process.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Install and enable the "**External Reset Password**" module.
2. Navigate to "**Configuration**" > "**People**" >
"**External Reset Password**".
3. Enter the URL of your preferred external password reset resource.
4. Clear your cache for the changes to take effect.

## Maintainers

- [Skilld](https://www.drupal.org/skilld)
